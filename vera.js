// Definition de tous les éléments du DOM
const txtEmail = document.getElementById('lemail_field');
const txtPassword = document.getElementById('lpassword_field');
const btnInfos = document.querySelector('.infos');
const btnAddPicture = document.querySelector('.pp');
const uPr = document.getElementById('user_para');
const uDetails = document.getElementById('user_details');
const uDiv = document.getElementById("user_div");
const lgDiv = document.getElementById("login_div");
const uPP = document.getElementById('u_pp');

// Fonction de connexion
function login() {
    var userEmail = txtEmail.value;
    var userPass = txtPassword.value;

    firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
    
        window.alert("Error : " + errorMessage);
    });
}

// Fonction de deconnexion
function logout(){
    firebase.auth().signOut();
}
// Fonction pour recuperer et afficher les informations de l'user
function getUserInfos(userId) {
    var userRef = firebase.database().ref("/users/" + userId);
        userRef.on('value', function (snapshot) {
            var data = snapshot.val();
            if(data.user_pp_url != '') {
                btnAddPicture.innerHTML = "Modifier la photo de profile";
            };
            uDetails.innerHTML = `
            <ul>
            <li>Nom complet: <b>${data.user_fullname}</b></li>
            <li>Age: <b>${data.user_age}</b></li>
            <li>Photo de profil: <br><img src="${data.user_pp_url}"></li>
            </ul>
            `;
        });
}
// Fonction pour ajouter/modifier la photo de profile
function addPP(userId) {
    uPP.innerHTML = `<h6>Choisi une image:</h6><input type="file" id="file" name="file"/>`;
    var storageRef = firebase.storage().ref();
    document.getElementById('file').addEventListener('change', function(evt){
        evt.stopPropagation();
        evt.preventDefault();
        var file = evt.target.files[0];

        var metadata = {
          'contentType': file.type
        };
        storageRef.child('images/'+ userId +'/'+ file.name).put(file, metadata).then(function(snapshot) {
            console.log('Uploaded', snapshot.totalBytes, 'bytes.');
            console.log(snapshot.metadata);
            var url = snapshot.downloadURL;
            console.log('File available at', url);
            //
            firebase.database().ref('users/' + userId).update({
                user_pp_url: url
            });
            alert("Votre image a été uploadé/enregistrer");
            uPP.style.display = "none";
            uDetails.style.display = "block";
            // 
        }).catch(function(error) {
            // 
            console.error('Upload failed:', error);
            // 
        });
    });
}

window.onload= function() {
    firebase.auth().onAuthStateChanged(function(user){
        if(user) {
            uDiv.style.display = "block";
            lgDiv.style.display = "none";
            var user = firebase.auth().currentUser;
            if(user != null) {
                var userId = user.uid;
                var email_id = user.email;
                uPr.innerHTML = `Tu est connecté en tant que <b>${email_id}</b>`;
                getUserInfos(userId);
                addPP(userId);
            }
        }
        else {
            // Aucun utilisateur est connecté
            uDiv.style.display = "none";    
            lgDiv.style.display = "block";
        };

    });
    btnInfos.addEventListener('click', function(e) {
        uDetails.style.display = "block";
        uPP.style.display = "none";
    });
    btnAddPicture.addEventListener('click', function(e) {
        uPP.style.display = "block";
        uDetails.style.display = "none";
    });
};

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      var fullName = document.getElementById("fullname_field").value;
      var userAge = document.getElementById("age_field").value;
      document.getElementById("user_div").style.display = "block";
      document.getElementById("register_div").style.display = "none";
      
  
      var user = firebase.auth().currentUser;
  
      if(user != null){
  
        var uid = user.uid;
        var email_id = user.email;
  
        firebase.database().ref('users/' + uid).set({
          user_id: uid,
          user_email: user.email,
          user_fullname: fullName,
          user_pp_url: '',
          user_age: userAge
          
        });
        document.getElementById("user_para").innerHTML = `Chargement...`;
        setTimeout(function(){
          window.location.href = "./index.html";
        }, 2000)
  
      }
  
    } else {  
      document.getElementById("user_div").style.display = "none";    
      document.getElementById("register_div").style.display = "block";
    }
  });

  function register(){
  
    var userEmail = document.getElementById("email_field").value;
    var userPass = document.getElementById("password_field").value;
  
  
    firebase.auth().createUserWithEmailAndPassword(userEmail, userPass).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
  
      window.alert("Error : " + errorMessage);
  
      // ...
    });
  
  
  };